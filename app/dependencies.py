from engines.storage import DBEngine
from rodi import Container

container = Container()

container.register(obj_type=DBEngine, instance=DBEngine())
