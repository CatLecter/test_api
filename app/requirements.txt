pydantic==1.10.13
fastapi==0.103.2
asyncpg==0.28.0
gunicorn==21.2.0
uvicorn==0.23.2
python-dotenv==1.0.0
rodi==2.0.3
orjson==3.9.7
